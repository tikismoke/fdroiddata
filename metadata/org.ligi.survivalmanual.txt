Categories:Reading
License:GPLv3
Web Site:https://github.com/ligi/SurvivalManual/blob/HEAD/README.md
Source Code:https://github.com/ligi/SurvivalManual
Issue Tracker:https://github.com/ligi/SurvivalManual/issues

Auto Name:Survival Manual
Summary:Learn how to survive
Description:
Survival Manual based on the Army Field Manual 21-76 - fully working offline
.

Repo Type:git
Repo:https://github.com/ligi/SurvivalManual

Build:1.0,1
    commit=1.0
    subdir=app
    gradle=yes

Auto Update Mode:None
Update Check Mode:Tags
Current Version:1.0
Current Version Code:1
